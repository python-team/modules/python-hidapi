Source: python-hidapi
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Soren Stoutner <soren@debian.org>
Section: python
Priority: optional
Homepage: https://github.com/trezor/cython-hidapi
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-sequence-python3,
               libhidapi-dev,
               libudev-dev,
               libusb-1.0-0-dev,
               pkgconf,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-setuptools
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/python-team/packages/python-hidapi.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-hidapi
Rules-Requires-Root: no

Package: python3-hid
Architecture: any
# libhidapi-hidraw0 is one of the two ways python-hidapi talks to hardware, the other being libusb and libudev.
Depends: libhidapi-hidraw0,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Provides: ${python3:Provides}
Description: cython3 interface to hidapi
 HIDAPI is a multi-platform library which allows an application to interface
 with USB and Bluetooth HID-Class devices.  HIDAPI can be either built as a
 shared library or can be embedded directly into a target application by adding
 a single source file (per platform) and a single header.
 .
 This package contains HIDAPI for Python 3.
